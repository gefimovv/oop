﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Geometry
{
    class ShapesBuilder
    {
        private ConsoleColor color;
        public ConsoleColor Color
        {
            get { return color; }
            set
            {
                color = value;
                Console.ForegroundColor = color;
            }
        }

        private char symbol;
        public char Symbol
        {
            get { return symbol; }
            set
            {
                symbol = value;
            }
        }

        public string Line(int lengte)
        {
            return new string(Symbol, lengte);
        }
        public string Rectangle(int height, int width)
        {
            string output = "";
            for (int i = 0; i < height; i++)
            {
                output += Line(width);
                if (i < height-1) { 
                output += "\n";
                }
            }
            return output;
        }
        public string Triangle(int height)
        {
            string output = "";
            for (int i = 0; i < height; i++)
            {
                output += Line(i+1);
                if (i < height - 1)
                {
                    output += "\n";
                }
            }
            return output;
        }
    }
}
