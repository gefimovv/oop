﻿using System;
using OOP.Geometry;
using OOP.Cars;

namespace OOProgrammeren
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice;
            while (true)
            {
            Console.WriteLine("Wat wil jij doen?");
            Console.WriteLine("1. Vormen Tekenen");
            Console.WriteLine("2. Auto's laten rijden");
            Console.WriteLine("3. Patiënten tonen");
            Console.WriteLine("4. Honden laten blaffen");
            Console.WriteLine("Geef een getal in: ");
            choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                        ShapesBuilder builder = new ShapesBuilder();
                        builder.Color = ConsoleColor.Green;
                        builder.Symbol = '*';
                        string line = builder.Line(10);
                        builder.Color = ConsoleColor.Red;
                        string rectangle = builder.Rectangle(10, 5);
                        Console.WriteLine(rectangle);
                break;
                case 2:
                        Car car1 = new Car();
                        for (int i = 0; i < 5; i++)
                        {
                            car1.Gas();
                        }
                        for (int i = 0; i < 3; i++)
                        {
                            car1.Brake();
                        }
                        Car car2 = new Car();
                        for (int i = 0; i < 5; i++)
                        {
                            car2.Gas();
                        }
                        for (int i = 0; i < 3; i++)
                        {
                            car2.Brake();
                        }
                        Console.WriteLine($"auto 1: {car1.Speed}km/u, afgelegde weg {car1.Odometer:f2}km");
                        Console.WriteLine($"auto 2: {car2.Speed}km/u, afgelegde weg {car2.Odometer:f2}km");
                        break;
                case 3: Console.WriteLine("Patiënten tonen");
                    break;
                case 4:
                    Console.WriteLine("Honden laten blaffen");
                    break;
                default: Console.WriteLine("Ongeldige Keuze");
                    break;
            }

            }


        }
    }
}
